import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";
import {BrowserModule} from "@angular/platform-browser";
import {AppComponent} from "./app.component";

@NgModule({
    imports:      [BrowserModule, FormsModule, BrowserModule, ReactiveFormsModule ],
    bootstrap:    [AppComponent],  // indicate the bootstrap component
    declarations: [AppComponent], // register our component with the module
})
export class AppModule {}
