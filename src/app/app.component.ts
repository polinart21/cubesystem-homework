import {Component} from "@angular/core";
import { FormBuilder,  FormGroup,  Validators } from "@angular/forms";

@Component({
    selector: "test-app",
    styles: [ require("./app.component.scss") ],
    template: require("./app.component.html"),
})
export class AppComponent {
    public angForm: FormGroup;
    constructor(private fb: FormBuilder) {
        this.createForm();
    }
    public createForm() {
        this.angForm = this.fb.group({
            address: [""],
            fullname: ["", Validators.required ],
            notice: ["", Validators.required ],
            phone: ["", Validators.required ],
        });
    }

    public doSubmit() {
        console.log("Submit the form", this.angForm.value);
    }
}
